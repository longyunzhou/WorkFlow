﻿using System;

namespace WikeSoft.Global
{
    /// <summary>
    /// 上传文件服务的实体对象
    /// </summary>
    [Serializable]
    public class FileEntity
    {
        /// <summary>
        /// 应用名称
        /// </summary>
        public string AppName { get; set; }
        /// <summary>
        /// 组别
        /// </summary>
        public string Group { get; set; }
        /// <summary>
        /// 文件类型（文件扩展名）
        /// </summary>
        public string FileType { get; set; }
        /// <summary>
        /// 文件内容
        /// </summary>
        public byte[] Contents { get; set; }
        /// <summary>
        /// 是否为图片文件
        /// </summary>
        public bool? IsImageFile { get; set; }
        /// <summary>
        /// 是否等比压缩，（值true时,ZoomOut、CompressWidth、CompressHight至少有一个值不能为空）
        /// </summary>
        public bool IsCompressImage { get; set; }
        /// <summary>
        /// 缩小倍数（将图片等比例压缩）
        /// </summary>
        public double? ZoomOut { get; set; }

        /// <summary>
        /// 压缩固定宽度（高度根据宽度自行计算）
        /// </summary>
        public int? CompressWidth { get; set; }

        /// <summary>
        /// 压缩高度（宽度根据高度自行计算）
        /// </summary>
        public int? CompressHight { get; set; }

    }
}
