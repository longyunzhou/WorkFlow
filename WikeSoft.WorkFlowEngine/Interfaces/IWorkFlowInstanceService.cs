﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WikeSoft.WorkFlowEngine.Filter;
using WikeSoft.WorkFlowEngine.Models;
using WikeSoft.WorkFlowEngine.Msg;

namespace WikeSoft.WorkFlowEngine.Interfaces
{
    public interface IWorkFlowInstanceService
    {
        /// <summary>
        /// 创建流程
        /// </summary>
        /// <param name="parms"></param>
        /// <returns></returns>
        InstanceMessage CreateInstance(CreateParms parms);


        /// <summary>
        /// 处理流程
        /// </summary>
        /// <param name="auditParams"></param>
        /// <returns></returns>
        InstanceMessage Complete(AuditParams auditParams);


        /// <summary>
        /// 审核，流程指定到某一个节点
        /// </summary>
        /// <param name="nodeRecordId">节点ID</param>
        /// <param name="targetNodeId">目标节点ID</param>
        /// <param name="userId">用户Id</param>
        /// <param name="userName">用户姓名</param>
        /// <param name="userMsg">用户评论</param>
        /// <param name="targetUserId"></param>
        /// <returns></returns>
        InstanceMessage CompleteToAppointNode(string nodeRecordId,string targetNodeId, string userId, string userName, string userMsg,string targetUserId);

      
        /// <summary>
        /// 得到当前运行的实例
        /// </summary>
        /// <param name="flowId">流程ID</param>
        /// <returns></returns>
        TaskInstance GetRunFlowInstance(string flowId);

        /// <summary>
        /// 得到历史流程处理记录列表
        /// </summary>
        /// <param name="flowId">流程ID</param>
        /// <returns></returns>
        List<TaskInstance> GetHistoryFlowInstances(string flowId);
        
        /// <summary>
        /// 得到运行中的流程图
        /// </summary>
        /// <param name="flowId"></param>
        /// <returns></returns>
        Bitmap GetRunBitmap(string flowId);

        /// <summary>
        /// 得到流程节点的转出条件
        /// </summary>
        /// <param name="nodeRecordId"></param>
        /// <returns></returns>
        List<TaskCondition> GetNodeOutConditions(String nodeRecordId);

        /// <summary>
        /// 得到流程的节点
        /// </summary>
        /// <param name="nodeRecordId"></param>
        /// <returns></returns>
        List<Models.Task> GetTaskNodes(String nodeRecordId);

        /// <summary>
        /// 得到流程的任务节点
        /// </summary>
        /// <param name="flowDefId">流程定义ID</param>
        /// <param name="version"></param>
        /// <returns></returns>
        List<Models.Task> GetFlowTasks(String flowDefId, int version);


        /// <summary>
        /// 创建实例，第一个环节的权限
        /// </summary>
        /// <param name="flowDefKey"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        WorkFlowAuthority GetFirstNodeAuthority(string flowDefKey, string condition);
        /// <summary>
        /// 得到下一节点的权限控件
        /// </summary>
        /// <param name="nodeRecordId"></param>
        /// <param name="condition"></param>
        /// <returns></returns>
        WorkFlowAuthority GetNextNodeAuthority(string nodeRecordId, string condition);

        /// <summary>
        /// 得到下一节点的权限控件
        /// </summary>
        /// <param name="nodeRecordId"></param> 
        /// <param name="targetNodeId"></param>
        /// <returns></returns>
        WorkFlowAuthority GetNodeAuthority(string nodeRecordId, string targetNodeId);
 

        List<WorkFlowAuthority> GetAuthorities(string flowDefKey);

            /// <summary>
        /// 得到用户待处理的流程实例
        /// </summary>
        /// <param name="targetUser"></param>
     
        /// <returns></returns>
        List<TaskInstance> GetRuningFlowInstancesAuthority(string targetUser);
        /// <summary>
        /// 删除流程实例
        /// </summary>
        /// <param name="flowId"></param>
        FlowMessage DeleteFlowInstance(string flowId);

        PagedResult<WorkFlowInstanceModel> Query(WorkFlowInstanceFilter filter);
 
    }
}
