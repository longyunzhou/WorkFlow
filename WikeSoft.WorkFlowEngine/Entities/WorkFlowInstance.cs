// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.51

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WikeSoft.WorkFlowEngine.Enum;

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace WikeSoft.WorkFlowEngine.Entities
{
 
    // WorkFlow_Instance
    [System.CodeDom.Compiler.GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "2.19.2.0")]
    [Table("WorkFlow_Instance")]
    public partial class WorkFlowInstance
    {

        ///<summary>
        /// 流程实例Id
        ///</summary>
        [Key]
        [MaxLength(50)]
       // [Column(TypeName = "varchar")]
        public string Id { get; set; } // Id (Primary key) (length: 50)

        ///<summary>
        /// 流程定义ID
        ///</summary>
       
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string WorkFlowDefId { get; set; } // WorkFlowDefId (length: 50)

        ///<summary>
        /// 实例名称
        ///</summary>
        
        [MaxLength(2000)]
        //[Column(TypeName = "varchar")]
        public string InstanceName { get; set; } // InstanceName (length: 2000)

        ///<summary>
        /// 创建人
        ///</summary>
       
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string CreateUser { get; set; } // CreateUser (length: 50)

        ///<summary>
        /// 创建人用户Id
        ///</summary> 
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string CreateUserId { get; set; } // CreateUserId (length: 50)

        ///<summary>
        /// 创建时间
        ///</summary>
        public System.DateTime CreateTime { get; set; } // CreateTime

        ///<summary>
        /// 流程状态(0：历史，1：运行)
        ///</summary>
        public FlowRunStatus RunStatus { get; set; } // RunStatus

        ///<summary>
        /// 版本
        ///</summary>
        public int Version { get; set; } // Version

        ///<summary>
        /// 业务Id
        ///</summary>
       
        [MaxLength(100)]
        //[Column(TypeName = "varchar")]
        public string ObjectId { get; set; } // ObjectId (length: 100)

        ///<summary>
        /// 关联用户
        ///</summary>
       
        [MaxLength(2000)]
        //[Column(TypeName = "varchar")]
        public string AssociatedUserId { get; set; } // AssociatedUserId (length: 2000)

        ///<summary>
        /// 其它参数（contractId=222&person=3k3...）
        ///</summary>
        
        [MaxLength(2000)]
        //[Column(TypeName = "varchar")]
        public string OtherParams { get; set; } // OtherParams (length: 255)

        // Reverse navigation
        public virtual System.Collections.Generic.ICollection<WorkFlowTask> WorkFlowTasks { get; set; } // WorkFlow_Task.FK_Flow_NodeRecord_Flow_FlowRecord

        // Foreign keys
        [ForeignKey("WorkFlowDefId")]
        public virtual WorkFlowDef WorkFlowDef { get; set; } // FK_Flow_FlowRecord_Flow_FlowDef
        
        public WorkFlowInstance()
        {
            WorkFlowTasks = new System.Collections.Generic.List<WorkFlowTask>();
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
