// <auto-generated>
// ReSharper disable ConvertPropertyToExpressionBody
// ReSharper disable DoNotCallOverridableMethodsInConstructor
// ReSharper disable InconsistentNaming
// ReSharper disable PartialMethodWithSinglePart
// ReSharper disable PartialTypeWithSinglePart
// ReSharper disable RedundantNameQualifier
// ReSharper disable RedundantOverridenMember
// ReSharper disable UseNameofExpression
// TargetFrameworkVersion = 4.51

using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using WikeSoft.WorkFlowEngine.Enum;

#pragma warning disable 1591    //  Ignore "Missing XML Comment" warning

namespace WikeSoft.WorkFlowEngine.Entities
{


    // WorkFlow_Task
    [System.CodeDom.Compiler.GeneratedCodeAttribute("EF.Reverse.POCO.Generator", "2.19.2.0")]
    [Table("WorkFlow_Task")]
    public partial class WorkFlowTask
    {

        ///<summary>
        /// 主键
        ///</summary>
        [Key]
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string Id { get; set; } // Id (Primary key) (length: 50)

        ///<summary>
        /// 流程实例Id
        ///</summary>
      
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string InstanceId { get; set; } // InstanceId (length: 50)

        ///<summary>
        /// 节点定义ID
        ///</summary>
       
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string NodeDefId { get; set; } // NodeDefId (length: 50)

        ///<summary>
        /// 处理日期
        ///</summary>
        public System.DateTime? DealDate { get; set; } // DealDate

        ///<summary>
        /// 处理人姓名
        ///</summary>
      
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string DealUser { get; set; } // DealUser (length: 50)

        ///<summary>
        /// 处理人用户Id
        ///</summary>
        
        [MaxLength(50)]
        //[Column(TypeName = "varchar")]
        public string DealUserId { get; set; } // DealUserId (length: 50)

        ///<summary>
        /// 处理意见
        ///</summary>
       
        [MaxLength(500)]
        //[Column(TypeName = "varchar")]
        public string DealRemark { get; set; } // DealRemark (length: 500)

        ///<summary>
        /// 状态（0，历史，1：执行）
        ///</summary>
        public FlowRunStatus RunStatus { get; set; } // RunStatus

        ///<summary>
        /// 下一环节流程处理人Id
        ///</summary>
       
        [MaxLength(8000)]
        //[Column(TypeName = "varchar")]
        public string TargetUserId { get; set; } // TargetUserId (length: 8000)

        // Foreign keys
        [ForeignKey("InstanceId")]
        public virtual WorkFlowInstance WorkFlowInstance { get; set; } // FK_Flow_NodeRecord_Flow_FlowRecord
        
        public WorkFlowTask()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
// </auto-generated>
