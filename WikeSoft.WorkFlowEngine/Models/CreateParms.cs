﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.WorkFlowEngine.Models
{
    public class CreateParms
    {
        /// <summary>
        /// 流程定义key
        /// </summary>
        public string FlowDefKey{get;set;}

        /// <summary>
        /// 创建人用户Id
        /// </summary>
        public string UserId{get;set;}
        /// <summary>
        /// 创建人
        /// </summary>
        public string UserName{get;set;}
        /// <summary>
        /// 条件
        /// </summary>
        public string Condition{get;set;}
        /// <summary>
        /// 目标用户
        /// </summary>
        public string TargetUserId{get;set;}
        /// <summary>
        /// 业务对象Id
        /// </summary>
        public string ObjectId{get;set;}
        /// <summary>
        /// 流程名称 
        /// </summary>
        public string InstanceName{get;set;}
        /// <summary>
        /// 说明
        /// </summary>
        public string Remark { get; set; }
        /// <summary>
        /// 整个业务流程相关的用户Ids
        /// </summary>
        public string AssociatedUserId { get; set; }

        /// <summary>
        /// 其它参数
        /// </summary>
        public string OtherParams { get; set; }
    }
}
