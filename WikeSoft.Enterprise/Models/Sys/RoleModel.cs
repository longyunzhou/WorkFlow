﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using WikeSoft.Core.Extension;

namespace WikeSoft.Enterprise.Models.Sys
{
    /// <summary>
    /// 角色
    /// </summary>
    public class RoleModel
    {
        ///<summary>
        /// 主键
        ///</summary>
        public string Id { get; set; } // Id (Primary key)

        ///<summary>
        /// 角色名
        ///</summary>
        public string RoleName { get; set; } // RoleName (length: 500)

        ///<summary>
        /// 说明
        ///</summary>
        public string Remark { get; set; } // Remark (length: 500)

        ///<summary>
        /// 创建时间
        ///</summary>
        public DateTime CreateDate { get; set; } // CreateDate

        ///<summary>
        /// 创建时间
        ///</summary>
        public string CreateDateString
        {
            get { return CreateDate.DefaultFormat(); }
        }

        /// <summary>
        /// 是否选中
        /// </summary>
        public bool Checked { get; set; }

        /// <summary>
        /// 关联的用户名，以“，”分开
        /// </summary>
        public String TrueNames { get; set; }
    }

    /// <summary>
    /// 添加角色模型
    /// </summary>
    public class RoleAddModel
    {
        ///<summary>
        /// 角色名
        ///</summary>
        [Display(Name = "角色名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(50, ErrorMessage = ModelErrorMessage.MaxLength)]
        [Remote("IsExists", "Role", ErrorMessage = ModelErrorMessage.Exists)]
        public string RoleName { get; set; } // RoleName (length: 500)

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "角色说明")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string Remark { get; set; } // Remark (length: 500)
    }

    /// <summary>
    /// 编辑角色模型
    /// </summary>
    public class RoleEditModel
    {
        [Required]
        public string Id { get; set; }

        ///<summary>
        /// 角色名
        ///</summary>
        [Display(Name = "角色名称")]
        [Required(ErrorMessage = ModelErrorMessage.Required)]
        [MaxLength(50, ErrorMessage = ModelErrorMessage.MaxLength)]
        [Remote("IsExists", "Role", AdditionalFields = "Id", ErrorMessage = ModelErrorMessage.Exists)]
        public string RoleName { get; set; } // RoleName (length: 500)

        ///<summary>
        /// 说明
        ///</summary>
        [Display(Name = "角色说明")]
        [MaxLength(500, ErrorMessage = ModelErrorMessage.MaxLength)]
        public string Remark { get; set; } // Remark (length: 500)
    }

    /// <summary>
    /// 设置角色权限模型
    /// </summary>
    public class SetRoleRightModel
    {
        /// <summary>
        /// 角色ID
        /// </summary>
        public string RoleId { get; set; }

        /// <summary>
        /// 页面权限ID
        /// </summary>
        public List<string> PageIds { get; set; }
    }
}
