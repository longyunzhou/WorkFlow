﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 借还证类型
    /// </summary>
    public enum BorrowReturnTypeEnum
    {
        /// <summary>
        /// 人才部借证
        /// </summary>
        [Description("人才部借还证")]
        CustomerDepartment = 0,

        /// <summary>
        /// 企业部借证
        /// </summary>
        [Description("企业部借还证")]
        CompanyDepartment = 1,

      
    }
}
