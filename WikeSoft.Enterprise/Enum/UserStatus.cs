﻿using System.ComponentModel;

namespace WikeSoft.Enterprise.Enum
{
    /// <summary>
    /// 用户状态
    /// </summary>
    public enum UserStatus
    {
        [Description("在职")]
        Enabled = 1,

        [Description("离职")]
        Disabled = 2,

         
    }
 
}
