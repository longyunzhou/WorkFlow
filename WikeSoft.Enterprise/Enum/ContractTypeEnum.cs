﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WikeSoft.Data.Enum
{
    public enum ContractTypeEnum
    {
        /// <summary>
        /// 预计合同
        /// </summary>
        [Description("预计合同")]
        Plan =0,

        /// <summary>
        /// 正式合同
        /// </summary>
        [Description("正式合同")]
        Official =1,

    }
}
