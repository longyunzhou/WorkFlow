﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{
    /// <summary>
    /// 客户证书状态枚举
    /// </summary>
    public enum CustomerCertificateStatusEnum
    {
        /// <summary>
        /// 资源客户
        /// </summary>
        [Description("资源客户")]
        Resource = 0,

        /// <summary>
        /// 意向客户
        /// </summary>
        [Description("意向客户")]
        Interested = 1,

        /// <summary>
        /// 已签客户
        /// </summary>
        [Description("已签客户")]
        Signed = 2,

        /// <summary>
        /// 库存客户
        /// </summary>
        [Description("库存客户")]
        Store = 3,

        /// <summary>
        /// 待做资料，，常规意义的转至受理
        /// </summary>
        [Description("待做资料")]
        WaitDoData = 4,
        /// <summary>
        /// 待交件
        /// </summary>
        [Description("待交件")]
        WaitSubmitData = 5,

        /// <summary>
        /// 待公示
        /// </summary>
        [Description("待公示")]
        WaitPublicity = 6,
        /// <summary>
        /// 待公告
        /// </summary>
        [Description("待公告")]
        WaitNotice = 7,

        /// <summary>
        /// 注册成功
        /// </summary>
        [Description("注册成功")]
        RegisterSucess = 8,

        /// <summary>
        /// 信誉不良
        /// </summary>
        [Description("信誉不良")]
        BadReputation =9,

        /// <summary>
        /// 入库申请
        /// </summary>
        [Description("入库申请")]
        StoreApply =10,
        
        /// <summary>
        /// 补入库申请
        /// </summary>
        [Description("补入库申请")]
        AgainStoreApply = 11, 

        /// <summary>
        /// 问题证书
        /// </summary>
        [Description("问题证书")]
        HaveQuestion = 12,

        /// <summary>
        /// 解除合作
        /// </summary>
        [Description("解除合作")]
        RelieveCooperation =13
    }
}
