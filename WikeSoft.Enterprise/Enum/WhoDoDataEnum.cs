﻿using System.ComponentModel;

namespace WikeSoft.Data.Enum
{
     
    public enum WhoDoDataEnum
    {
        [Description("单位做资料")]
        Other = 0,

        [Description("我方做资料")]
        Me = 1
    }
}
