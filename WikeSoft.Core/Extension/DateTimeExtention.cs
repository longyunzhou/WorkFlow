﻿using System;
using System.Globalization;

namespace WikeSoft.Core.Extension
{
    public static class DateTimeExtention
    {
        /// <summary>
        /// 默认格式化
        /// </summary>
        /// <param name="date"></param>
        /// <param name="defaultFormat">默认格式</param>
        /// <returns></returns>
        public static string DefaultFormat(this DateTime date,string defaultFormat = "yyyy-MM-dd HH:mm:ss")
        {
            return date.ToString(defaultFormat);
        }

        /// <summary>
        /// 默认格式化
        /// </summary>
        /// <param name="date"></param>
        /// <param name="defaultFormat">默认格式</param>
        /// <returns></returns>
        public static string DefaultFormat(this DateTime? date, string defaultFormat = "yyyy-MM-dd HH:mm:ss")
        {
            if (date.HasValue)
                return date.Value.ToString(defaultFormat);
            return string.Empty;
        }

        /// <summary>
        /// 得到年份
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static int GetYear(this DateTime datetime)
        {
            DateTime date = datetime.GetThisWeekMonday();
            return date.Year;
        }

        /// <summary>
        /// 得到时间对应的星期一
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static DateTime GetThisWeekMonday(this DateTime date)
        {
            DateTime firstDate = System.DateTime.Now;
            switch (date.DayOfWeek)
            {
                case System.DayOfWeek.Monday:
                    firstDate = date;
                    break;
                case System.DayOfWeek.Tuesday:
                    firstDate = date.AddDays(-1);
                    break;
                case System.DayOfWeek.Wednesday:
                    firstDate = date.AddDays(-2);
                    break;
                case System.DayOfWeek.Thursday:
                    firstDate = date.AddDays(-3);
                    break;
                case System.DayOfWeek.Friday:
                    firstDate = date.AddDays(-4);
                    break;
                case System.DayOfWeek.Saturday:
                    firstDate = date.AddDays(-5);
                    break;
                case System.DayOfWeek.Sunday:
                    firstDate = date.AddDays(-6);
                    break;
            }
            return Convert.ToDateTime(firstDate.ToShortDateString());
        }


        /// <summary>
        /// 得到时间对应的年份的周数
        /// </summary>
        /// <param name="date">当前时间</param>
        /// <returns>当前时间所在年的第多少周</returns>
        public static int WeekOfYear(this DateTime date)
        {
            GregorianCalendar gc = new GregorianCalendar();
            return gc.GetWeekOfYear(date, CalendarWeekRule.FirstFullWeek, DayOfWeek.Monday);
        }


        public static string GetDayOfWeek(this DateTime dt)
        {
            string ret = string.Empty;
            //DayOfWeek.Monday==
            if (DayOfWeek.Monday == dt.DayOfWeek)
            {
                ret = "星期一";
            }
            else if (DayOfWeek.Tuesday == dt.DayOfWeek)
            {
                ret = "星期二";
            }
            else if (DayOfWeek.Wednesday == dt.DayOfWeek)
            {
                ret = "星期三";
            }
            else if (DayOfWeek.Thursday == dt.DayOfWeek)
            {
                ret = "星期四";
            }
            else if (DayOfWeek.Friday == dt.DayOfWeek)
            {
                ret = "星期五";
            }
            else if (DayOfWeek.Saturday == dt.DayOfWeek)
            {
                ret = "星期六";
            }
            else if (DayOfWeek.Sunday == dt.DayOfWeek)
            {
                ret = "星期日";
            }
            return ret;
        }
    }
   
}
