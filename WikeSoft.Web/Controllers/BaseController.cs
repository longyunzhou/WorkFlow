﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.Web.Routing;

using WikeSoft.Enterprise.Interfaces.Sys;

namespace WikeSoft.Web.Controllers
{
    [WikeAuthorize]
    [PasswordExpiration]
    public class BaseController : Controller
    {


       

        private readonly IDataDictionaryService _dataDictionaryService;

        public BaseController()
        {
           
            _dataDictionaryService = DependencyResolver.Current.GetService<IDataDictionaryService>();
        }

        protected IList<SelectListItem> GetDataDictionary(String groupCode)
        {
            return _dataDictionaryService.GetByGroupCode(groupCode);
        }

        protected override void Initialize(RequestContext requestContext)
        {
            if (requestContext.HttpContext.Request.Url != null)
            {
                var url = requestContext.HttpContext.Request.Url.AbsolutePath.ToLower();
              
                
            }
            base.Initialize(requestContext);
        }
        // GET: Base
        /// <summary>
        /// 成功
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult Ok(object data = null)
        {
            return Json(new { success = true, data }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 成功,原样返回data的值
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public JsonResult JsonOk(object data)
        {
            return Json(data, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// 失败
        /// </summary>
        /// <param name="msg">提示消息</param>
        /// <returns></returns>
        public JsonResult Fail(string msg = "操作失败")
        {
            return Json(new { success = false, message = msg }, JsonRequestBehavior.AllowGet);
        }

        /// <summary>
        /// json
        /// </summary>
        /// <param name="success">是否操作成功</param>
        /// <returns></returns>
        public JsonResult Json(bool success)
        {
            var msg = success ? "操作成功" : "操作失败";
            return Json(new { success, message = msg }, JsonRequestBehavior.AllowGet);
        }
    }
}