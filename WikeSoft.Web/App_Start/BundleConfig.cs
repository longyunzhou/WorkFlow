﻿using System.Web.Optimization;

namespace WikeSoft.Web
{
    public class BundleConfig
    {
        // 有关绑定的详细信息，请访问 http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //清除所有的忽略规则，让bundle可以打包文件名中还有.min.js/.min.css类型的文件
            bundles.IgnoreList.Clear();

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/Bootstrap/bootstrap.min.css",
                "~/Content/Bootstrap/bootstrap-responsive.min.css",
                "~/Content/Bootstrap/bootstrap-image-gallery.min.css",
                "~/Scripts/jqueryfileupload/jquery.fileupload-ui.css",
                "~/Content/Bootstrap/bootstrap-image-gallery.min.css",
                "~/Content/site.css"));

            bundles.Add(new ScriptBundle("~/bundles/fileuploader").Include(
                "~/Scripts/jquery-1.7.1.js",
                "~/Scripts/jquery-ui-1.8.20.min.js",
                "~/Scripts/jqueryfileupload/tmpl.min.js",
                "~/Scripts/jqueryfileupload/canvas-to-blob.min.js",
                "~/Scripts/jqueryfileupload/load-image.min.js",
                "~/Content/Bootstrap/bootstrap-image-gallery.min.js",
                "~/Content/Bootstrap/bootstrap.min.js",
                "~/Scripts/jqueryfileupload/jquery.iframe-transport.js",
                "~/Scripts/jqueryfileupload/jquery.fileupload.js",
                "~/Scripts/jqueryfileupload/jquery.fileupload-ip.js",
                "~/Scripts/jqueryfileupload/jquery.fileupload-ui.js",
                "~/Scripts/jqueryfileupload/locale.js",
                "~/Scripts/jqueryfileupload/main.js"));
        }
    }
}
