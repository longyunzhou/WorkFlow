﻿var WikePage = {
    GoTo: function (btn, url) {
        $(btn).button("loading");
        window.location.href = url;
    },

    //搜索jqGrid
    Search: function (json) {
        var postData = $("#table_list").jqGrid("getGridParam", "postData");
        $.extend(postData, json);
        $("#table_list").setGridParam({ search: true }).trigger("reloadGrid", [{ page: 1 }]);
    },

    DoPost: function (btn, url, data, sucCallback, failCallback) {
        var hasBtn = btn != null;
        if (hasBtn) {
            $(btn).button("loading");
        }
        var myRequestAjax = $.ajax({
            url: url,
            type: "POST",
            data: data,
            dataType: "JSON",
            success: function (res) {
                if (hasBtn) {
                    $(btn).button("reset");
                }
                if (res.success) {
                    if (sucCallback == null || typeof (sucCallback) == 'undefined')
                        parent.layer.alert("操作成功");
                    else
                        sucCallback.call(this, res);
                } else {
                    if (failCallback == null || typeof (failCallback) == 'undefined')
                        parent.layer.alert("操作失败：" + res.message);
                    else
                        failCallback.call(this, res);
                }
            },
            complete: function (xmlHttpRequest, status) { //请求完成后最终执行参数
                if (hasBtn) {
                    $(btn).button("reset");
                }
                if (status === "timeout") {//超时,status还有success,error等值的情况
                    myRequestAjax.abort();
                    parent.layer.alert("请求超时，请刷新页面重试");
                }
                if (status === "error") {
                    myRequestAjax.abort();
                    parent.layer.alert("请求失败，请刷新页面重试");
                }
            }
        });
    }
}