﻿

function printStart(data) {
    LODOP = getLodop();
    LODOP.PRINT_INIT("");
  
    LODOP.ADD_PRINT_RECT(50, 55, 680, 220, 0, 1);
    
    LODOP.ADD_PRINT_LINE(150, 55, 150, 736, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 155, 270, 155, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(100, 255, 100, 600, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 255, 270, 255, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 600, 270, 600, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(100, 420, 270, 420, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.SET_PRINT_STYLE("FontSize", 11);
    LODOP.ADD_PRINT_TEXT(20, 180, 500, 25, data.PostName + data.SYear + "年初“三公经费”预算审查情况报表");
    
   
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "Horient", 2);
    LODOP.ADD_PRINT_TEXT(90, 68, 100, 25, "公务接待费");
    LODOP.ADD_PRINT_TEXT(90, 165, 100, 25, "因公出国(境)费用");
    LODOP.ADD_PRINT_TEXT(65, 320, 150, 25, "公务用车");
    LODOP.ADD_PRINT_TEXT(120, 280, 150, 25, "维护费");
    LODOP.ADD_PRINT_TEXT(120, 450, 150, 25, "租赁费");
    LODOP.ADD_PRINT_TEXT(90, 620, 150, 25, "是否审核");
    LODOP.ADD_PRINT_TEXT(200, 68, 150, 25, data.Gwjdf);
    LODOP.ADD_PRINT_TEXT(200, 168, 150, 25, data.Ygcgf);
    LODOP.ADD_PRINT_TEXT(200, 280, 150, 25, data.Gwycf);
    LODOP.ADD_PRINT_TEXT(200, 450, 150, 25, data.Zpf);
    LODOP.ADD_PRINT_TEXT(200, 620, 150, 25, data.IsAudit===1?"是":"否");
    LODOP.PREVIEW();
}

function printEnd(data) {
    LODOP = getLodop();
    LODOP.PRINT_INIT("");

    LODOP.ADD_PRINT_RECT(50, 55, 680, 220, 0, 1);

    LODOP.ADD_PRINT_LINE(150, 55, 150, 736, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 155, 270, 155, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(100, 255, 100, 600, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 255, 270, 255, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 600, 270, 600, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(100, 420, 270, 420, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.SET_PRINT_STYLE("FontSize", 11);
    LODOP.ADD_PRINT_TEXT(20, 180, 500, 25, data.PostName + data.SYear + "年终“三公经费”决算审查情况报表");


    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "Horient", 2);
    LODOP.ADD_PRINT_TEXT(90, 68, 100, 25, "公务接待费");
    LODOP.ADD_PRINT_TEXT(90, 165, 100, 25, "因公出国(境)费用");
    LODOP.ADD_PRINT_TEXT(65, 320, 150, 25, "公务用车");
    LODOP.ADD_PRINT_TEXT(120, 280, 150, 25, "维护费");
    LODOP.ADD_PRINT_TEXT(120, 450, 150, 25, "租赁费");
    LODOP.ADD_PRINT_TEXT(90, 620, 150, 25, "是否审核");
    LODOP.ADD_PRINT_TEXT(200, 68, 150, 25, data.Gwjdf);
    LODOP.ADD_PRINT_TEXT(200, 168, 150, 25, data.Ygcgf);
    LODOP.ADD_PRINT_TEXT(200, 280, 150, 25, data.Gwycf);
    LODOP.ADD_PRINT_TEXT(200, 450, 150, 25, data.Zpf);
    LODOP.ADD_PRINT_TEXT(200, 620, 150, 25, data.IsAudit === 1 ? "是" : "否");
    LODOP.PREVIEW();
}

function printMonth(data) {
   LODOP = getLodop();
    LODOP.PRINT_INIT("");
  
    LODOP.ADD_PRINT_RECT(50, 55, 680, 220, 0, 1);
    
    LODOP.ADD_PRINT_LINE(150, 55, 150, 736, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 155, 270, 155, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(100, 255, 100, 600, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 255, 270, 255, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(50, 600, 270, 600, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(100, 420, 270, 420, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.SET_PRINT_STYLE("FontSize", 11);
    LODOP.ADD_PRINT_TEXT(20, 180, 500, 25, data.PostName + data.SYear + "年"+data.SMonth +"月“三公经费”使用审查情况报表");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "Horient", 2);

    LODOP.ADD_PRINT_TEXT(90, 68, 100, 25, "公务接待费");
    LODOP.ADD_PRINT_TEXT(90, 165, 100, 25, "因公出国(境)费用");
    LODOP.ADD_PRINT_TEXT(65, 320, 150, 25, "公务用车");
    LODOP.ADD_PRINT_TEXT(120, 280, 150, 25, "维护费");
    LODOP.ADD_PRINT_TEXT(120, 450, 150, 25, "租赁费");
    LODOP.ADD_PRINT_TEXT(90, 620, 150, 25, "是否审核");
    LODOP.ADD_PRINT_TEXT(200, 68, 150, 25, data.Gwjdf);
    LODOP.ADD_PRINT_TEXT(200, 168, 150, 25, data.Ygcgf);
    LODOP.ADD_PRINT_TEXT(200, 280, 150, 25, data.Gwycf);
    LODOP.ADD_PRINT_TEXT(200, 450, 150, 25, data.Zpf);
    LODOP.ADD_PRINT_TEXT(200, 620, 150, 25, data.IsAudit===1?"是":"否");
  


    //附表
    LODOP.ADD_PRINT_TEXT(400, 180, 500, 25, "接待是否规范审查情况（按月报送）");
    LODOP.SET_PRINT_STYLEA(0, "FontSize", 15);
    LODOP.SET_PRINT_STYLEA(0, "ItemType", 1);
    LODOP.SET_PRINT_STYLEA(0, "Horient", 2);

    LODOP.ADD_PRINT_RECT(430, 55, 680, 220, 0, 1);

    LODOP.ADD_PRINT_LINE(550, 55, 550, 736, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(430, 200, 650, 200, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距
    LODOP.ADD_PRINT_LINE(430, 480, 650, 480, 0, 1);//起点上边距，起点左边距，终点上边距，终点左边距

    LODOP.ADD_PRINT_TEXT(480, 75, 150, 25, "是否规范");
    LODOP.ADD_PRINT_TEXT(480, 220, 300, 25, "备注（附不规范原因）");
    LODOP.ADD_PRINT_TEXT(480, 500, 150, 25, "整改情况");

    LODOP.ADD_PRINT_TEXT(600, 75, 150, 25, data.IsStandard === 1 ? "是" : "否");
    LODOP.ADD_PRINT_TEXT(570, 220, 250, 100, data.Remark);
    LODOP.ADD_PRINT_TEXT(570, 500, 250, 100, data.Correct);
    LODOP.PREVIEW();

}