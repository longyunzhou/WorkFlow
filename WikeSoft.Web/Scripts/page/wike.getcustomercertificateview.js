﻿function openDialog(dom) {
    top.layer.open({
        title: '证书选择',
        type: 2,
        content: "/CustomerCertificate/SelectCustomerView",
        area: ['1250px', '650px'],
        btn: ['确认', '关闭'],
        btnclass: ['btn btn-primary', 'btn btn-danger'],
        yes: function (index, layero) {
            var companyCertificateId = getQueryString("CompanyCertificateId");

            var data = $(layero).find("iframe")[0].contentWindow.getContent();
            if (data.Data == null || data.Data.length == 0) {
                top.layer.alert("请选择数据");
                return;
            }

            $.ajax({
                url: '/CompanyCertificate/BuildRelactionWithCustomerCertificate',
                type: "POST",
                dataType: "json",
                data: JSON.stringify({ customerCertificateIds: data.Data, companyCertificateId: companyCertificateId, whoDoData: data.WhoDoData }),
                contentType: "application/json, charset=utf-8",
                success: function (data) {
                  
                    if (data.success) {
                        WikePage.Search({});
                        top.layer.close(index);
                    } else {
                        top.layer.alert("操作失败：" + data.message);
                    }
                }
            });

           
           
        }, cancel: function () {
            return true;
        }
    });
}

  